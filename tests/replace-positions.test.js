const replacePositions = require('../lib/replace-positions')

test('Replaces positions for a given letter in a word', () => {
  expect(replacePositions('testing', [0, 3, 4], 'x')).toEqual('xesxxng');
})
